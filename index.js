// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
module.exports = {
  root: true,

  parser: 'babel-eslint',

  parserOptions: {
    ecmaVersion: 2018,
    sourceType: 'module',
  },

  env: {
    browser: true,
    commonjs: true,
    es6: true,
    jest: true,
    node: true,
  },

  extends: [
    '@commonground/eslint-config/rules/generic',
    '@commonground/eslint-config/rules/header',
    '@commonground/eslint-config/rules/import',
    '@commonground/eslint-config/rules/jest',
    '@commonground/eslint-config/rules/reactAppGenerics',
    '@commonground/eslint-config/rules/security',
    'plugin:security/recommended',
    'standard',
    '@commonground/eslint-config/rules/prettier',
  ],
}
