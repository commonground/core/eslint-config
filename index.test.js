const fs = require('fs')

const files = fs.readdirSync(`${__dirname}/rules`)

describe('js rule files do not throw', () => {
  test('index', () => {
    expect(() => require('./index')).not.toThrow()
  })

  files.forEach((file) => {
    test(file, () => {
      expect(() => require(`./rules/${file}`)).not.toThrow()
    })
  })
})
