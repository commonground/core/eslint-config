# [8.0.0](https://gitlab.com/commonground/core/eslint-config/compare/v7.0.4...v8.0.0) (2022-03-14)


### Build System

* upgrade eslint-plugin-jest v25.x -> v26.x ([6a2d1e2](https://gitlab.com/commonground/core/eslint-config/commit/6a2d1e2012085028b9e942c82e1eb7a0d56f39f8))


### BREAKING CHANGES

* See changelog https://github.com/jest-community/eslint-plugin-jest

## [7.0.4](https://gitlab.com/commonground/core/eslint-config/compare/v7.0.3...v7.0.4) (2022-03-14)

## [7.0.3](https://gitlab.com/commonground/core/eslint-config/compare/v7.0.2...v7.0.3) (2022-03-02)

## [7.0.2](https://gitlab.com/commonground/core/eslint-config/compare/v7.0.1...v7.0.2) (2022-02-22)


### Bug Fixes

* remove deprecated jest rules ([59ec891](https://gitlab.com/commonground/core/eslint-config/commit/59ec891941ed5bade0904f28233eaec7131dd5c9))

## [7.0.1](https://gitlab.com/commonground/core/eslint-config/compare/v7.0.0...v7.0.1) (2022-02-18)


### Bug Fixes

* upgrade copyright linter to 2022 ([6dcc779](https://gitlab.com/commonground/core/eslint-config/commit/6dcc779ca66e691696839221ce6b27a215e6eb49))

# [7.0.0](https://gitlab.com/commonground/core/eslint-config/compare/v6.0.0...v7.0.0) (2022-01-10)


### Build System

* revert upgrade of eslint-plugin-promise ([1b7c9d7](https://gitlab.com/commonground/core/eslint-config/commit/1b7c9d7a9cea0650f84a55d5ed4fc09d4ad58fc7))
* update eslint-plugin-promise to v6.x ([ed79713](https://gitlab.com/commonground/core/eslint-config/commit/ed79713a76c925e4f76caabced65c464b196148a))


### BREAKING CHANGES

* downgraded from v6.x to v4.x because of conflicting peerDependencies. We should be able to upgrade once eslint-config-standard v17 is out.

See https://github.com/standard/eslint-config-standard/pull/193
* upgrade eslint-plugin-promise to v6.x

# [6.0.0](https://gitlab.com/commonground/core/eslint-config/compare/v5.0.1...v6.0.0) (2022-01-10)


### Build System

* update eslint-plugin-jest to v25.x ([afca80f](https://gitlab.com/commonground/core/eslint-config/commit/afca80f6f5389334429010a187e695c6ae7b0868))
* update eslint-plugin-prettier to v4.x ([7509e67](https://gitlab.com/commonground/core/eslint-config/commit/7509e67d01b8ae0d61d8c0edb3197a424be90762))


### BREAKING CHANGES

* upgrade eslint-plugin-prettier to v4.x
* upgrade eslint-plugin-jest to v25.x

## [5.0.1](https://gitlab.com/commonground/core/eslint-config/compare/v5.0.0...v5.0.1) (2022-01-10)

# [5.0.0](https://gitlab.com/commonground/core/eslint-config/compare/v4.2.1...v5.0.0) (2021-02-24)


### Build System

* upgrade eslint-config-prettier ([3377f7e](https://gitlab.com/commonground/core/eslint-config/commit/3377f7e856b427c0944c5f2d48f3b6a391a1bce6))


### BREAKING CHANGES

* eslint-config-prettier v6.x -> v8.x
https://github.com/prettier/eslint-config-prettier/blob/main/CHANGELOG.md

## [4.2.1](https://gitlab.com/commonground/core/eslint-config/compare/v4.2.0...v4.2.1) (2021-02-08)


### Bug Fixes

* change plugin order to prevent rule conflicts ([62937ea](https://gitlab.com/commonground/core/eslint-config/commit/62937ea846d8d420e64de346c4edaa8e31c44ec1))

# [4.2.0](https://gitlab.com/commonground/core/eslint-config/compare/v4.1.1...v4.2.0) (2021-01-11)


### Features

* update copyright year 2020 -> 2021 for file header ([c282295](https://gitlab.com/commonground/core/eslint-config/commit/c28229586ec027281372c620684af5d7454d15f0))

## [4.1.1](https://gitlab.com/commonground/core/eslint-config/compare/v4.1.0...v4.1.1) (2021-01-06)


### Bug Fixes

* version mismatch with config-cra ([7d64534](https://gitlab.com/commonground/core/eslint-config/commit/7d64534d40d63594bccc0e9aef10059e04f431db))

# [4.1.0](https://gitlab.com/commonground/core/eslint-config/compare/v4.0.2...v4.1.0) (2020-11-23)


### Features

* warn in dev for some rules ([e72309e](https://gitlab.com/commonground/core/eslint-config/commit/e72309e3dd10c613f381e3ab7b30cf9cb5aeef52))

## [4.0.2](https://gitlab.com/commonground/core/eslint-config/compare/v4.0.1...v4.0.2) (2020-11-05)


### Bug Fixes

* add eslint-import-resolver-node to peerDependencies ([8e9f6b1](https://gitlab.com/commonground/core/eslint-config/commit/8e9f6b161dc481e136a6f3dba8c438cd40d292a8))

## [4.0.1](https://gitlab.com/commonground/core/eslint-config/compare/v4.0.0...v4.0.1) (2020-10-29)


### Bug Fixes

* trigger patch release ([bf2f688](https://gitlab.com/commonground/core/eslint-config/commit/bf2f68805b76993d031b79ad79a6008dc7a7d7ed))

# [4.0.0](https://gitlab.com/commonground/core/eslint-config/compare/v3.0.0...v4.0.0) (2020-10-01)


### Features

* add eslint-plugin-security to rules ([ab27116](https://gitlab.com/commonground/core/eslint-config/commit/ab271164b9575d8f7f2535000f76af4491954d9b))


### BREAKING CHANGES

* when a security issue occurs,
the linting output will result in errors

# [3.0.0](https://gitlab.com/commonground/core/eslint-config/compare/v2.0.0...v3.0.0) (2020-09-10)


### Build System

* upgrade to ESLint v7.x and eslint-plugin-jest to v14.x ([024d7bc](https://gitlab.com/commonground/core/eslint-config/commit/024d7bce0cdb3f246c3f4a6dbe35c27fd9b1a4c9))


### BREAKING CHANGES

* ESLint changelog:
https://eslint.org/docs/user-guide/migrating-to-7.0.0

eslint-plugin-jest changelog:
https://github.com/jest-community/eslint-plugin-jest/releases/tag/v24.0.0

# [2.0.0](https://gitlab.com/commonground/core/eslint-config/compare/v1.0.0...v2.0.0) (2020-09-07)


### Features

* trigger major release ([6b6b546](https://gitlab.com/commonground/core/eslint-config/commit/6b6b546903acce8b3e1528f9801b09ec7eab082c))


### BREAKING CHANGES

* fix semantic-release

# 1.0.0 (2020-09-07)


### Features

* 1.0 release ([391de83](https://gitlab.com/commonground/core/eslint-config/commit/391de836049039740624471345b90fd4301c0b3d))
* project setup ([21e5451](https://gitlab.com/commonground/core/eslint-config/commit/21e545185561a378531f0514a43aa460affc07c6))

# 1.0.0 (2020-09-07)


### Features

* 1.0 release ([391de83](https://gitlab.com/commonground/core/eslint-config/commit/391de836049039740624471345b90fd4301c0b3d))
* project setup ([21e5451](https://gitlab.com/commonground/core/eslint-config/commit/21e545185561a378531f0514a43aa460affc07c6))
