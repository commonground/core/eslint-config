# Shared eslint-config

Common Ground shared ESLint config for all our JavaScript projects.

> For a create-react-app project see [our CRA lint config](https://gitlab.com/commonground/core/eslint-config-cra-standard-prettier#readme).

## Usage

Note: if you use a NPM version <7.x you should install the peer dependencies manually.

```shell
npm install --save-dev --save-exact @commonground/eslint-config
```

Then add the `eslintConfig` setting in `package.json`:

```json
"eslintConfig": {
  "extends": "@commonground/eslint-config"
}
```

Alternatively, you can remove the `eslintConfig` entry and create a `.eslintrc.js` file, with:

```js
module.exports = {
  extends: ['@commonground/eslint-config']
}
```

You can also extend only parts of our config, for instance to only use/add our `prettier` and `header` config to your linting:

```js
// .eslintrc.js
module.exports = {
  extends: [
    '@commonground/eslint-config/rules/prettier',
    '@commonground/eslint-config/rules/header',
  ]
}
```

Related plugins will automatically be added, so you don't need to add eg: `plugins: ['header']` yourself.

## Editor integration

### VSCode

Add the following extension: 

Then create the following file in the frontend project directory: `.vscode/settings.json` (it's .gitignored) containing:

```json
{
  "editor.defaultFormatter": "dbaeumer.vscode-eslint",
  "editor.codeActionsOnSave": {
    "source.fixAll.eslint": true
  }
}
```

You may define this file at a higher level as well, but then make sure you add this to the settings.json:

```json
{
  "eslint.workingDirectories": [
    "./ui-directory"
  ]
}
```

## How to contribute

See [CONTRIBUTING](https://gitlab.com/commonground/core/eslint-config/-/blob/master/CONTRIBUTING.md)
