// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
module.exports = {
  plugins: ['jest'],
  rules: {
    'jest/consistent-test-it': [
      'error',
      {
        fn: 'test',
        withinDescribe: 'it',
      },
    ],
    'jest/expect-expect': [
      'error',
      {
        assertFunctionNames: ['expect'],
      },
    ],
    'jest/no-jasmine-globals': 'error',
    'jest/no-done-callback': 'error',
    'jest/prefer-to-contain': 'error',
    'jest/prefer-to-have-length': 'error',
    'jest/valid-describe-callback': 'error',
    'jest/valid-expect-in-promise': 'error',
  }
}
