// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
module.exports = {
  plugins: ['prettier'],
  extends: [
    'plugin:prettier/recommended',
  ],
  rules: {
    'prettier/prettier': [
      'error',
      {
        tabWidth: 2,
        useTabs: false,
        semi: false,
        singleQuote: true,
        trailingComma: 'all',
        arrowParens: 'always',
      },
    ],
  }
}
