// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
module.exports = {
  plugins: ['header'],
  rules: {
    'header/header': [
      2,
      'line',
      [
        {
          pattern: ' Copyright © VNG Realisatie \\d{4}$',
          template: ' Copyright © VNG Realisatie 2022',
        },
        ' Licensed under the EUPL',
        '',
      ],
    ],
  }
}
