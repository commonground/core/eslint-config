// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
module.exports = {
  rules: {
    'security/detect-object-injection': 'error',
  }
}
