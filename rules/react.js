// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
module.exports = {
  rules: {
    'react/no-unsafe': 'warn',
    'react/forbid-prop-types': ['error', { forbid: ['any'] }],
    'react/jsx-handler-names': 'warn', // TODO: change to ERROR after migration of all projects
    'react/jsx-pascal-case': ['error', { allowAllCaps: true }],
    'react/sort-comp': 'warn', // TODO: change to ERROR after migration of all projects
  }
}
