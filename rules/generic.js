// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
const isDev =
  process &&
  process.env &&
  process.env.NODE_ENV &&
  process.env.NODE_ENV === 'development'

module.exports = {
  rules: {
    'no-console': isDev
      ? ['warn', { allow: ['error', 'warn'] }]
      : ['error', { allow: ['error', 'warn'] }],
    'no-unused-vars': isDev ? 'warn' : 'error',
  }
}
