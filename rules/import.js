// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
module.exports = {
  plugins: ['import'],
  rules: {
    'import/order': 'error',
    'import/first': 'error',
    'import/no-amd': 'error',
    'import/no-webpack-loader-syntax': 'error',
  }
}
